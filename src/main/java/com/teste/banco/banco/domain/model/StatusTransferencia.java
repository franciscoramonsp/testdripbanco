package com.teste.banco.banco.domain.model;

public enum StatusTransferencia {

    EM_ANALISE,REALIZADA,FALHA_AO_FINALIZAR_OPERACAO, FALHA_POR_EXCEDER_LIMITE, FALHA_POR_FALTA_DE_SALDO
}
