package com.teste.banco.banco.domain.repository;

import com.teste.banco.banco.domain.model.Conta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ContaRepository extends JpaRepository<Conta, Long> {

    @Query("select c from Conta c " +
            "where c.agencia = :agencia " +
            "and c.numero = :numero " +
            "and c.cliente.instituicao.id = :instituicao")
    Optional<Conta> findByAgenciaNumeroInstituicao(String agencia, String numero, Long instituicao);
}
