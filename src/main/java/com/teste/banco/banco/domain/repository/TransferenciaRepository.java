package com.teste.banco.banco.domain.repository;

import com.teste.banco.banco.domain.model.Transferencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransferenciaRepository extends JpaRepository<Transferencia,Long> {

    @Query("select t from Transferencia t where t.contaOrigem.id = :id or t.contaDestino.id = :id")
    List<Transferencia> findOfConta(Long id);
}
