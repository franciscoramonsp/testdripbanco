package com.teste.banco.banco.domain.model;

import java.math.BigDecimal;

public enum TipoTransferencia {

    INTERINSTITUCIONAL{
        @Override
        public BigDecimal getValorMaximo() {
            return BigDecimal.valueOf(5000);
        }

        @Override
        public BigDecimal getTaxa() {
            return BigDecimal.valueOf(5);
        }
    },INTRAINSTITUCIONAL{
        @Override
        public BigDecimal getValorMaximo() {
            return null;
        }

        @Override
        public BigDecimal getTaxa() {
            return BigDecimal.ZERO;
        }
    };

    public abstract BigDecimal getValorMaximo();

    public abstract BigDecimal getTaxa();
}
