package com.teste.banco.banco.domain.model;

import jakarta.persistence.*;

import java.math.BigDecimal;

@Entity
public class Conta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String agencia;
    private String numero;
    @ManyToOne
    private Cliente cliente;
    private BigDecimal saldo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public void subtrairSaldo(BigDecimal valor) {
        this.saldo = this.saldo.subtract(valor);
        if(this.saldo.signum()==-1) throw new RuntimeException("Saldo Insuficiente");
    }

    public void adicionarSaldo(BigDecimal valor) {
        this.saldo=this.saldo.add(valor);
    }
}
