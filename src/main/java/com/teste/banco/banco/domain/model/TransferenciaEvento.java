package com.teste.banco.banco.domain.model;

import org.springframework.context.ApplicationEvent;

public class TransferenciaEvento extends ApplicationEvent {

    private Long transferencia;

    public TransferenciaEvento(Long transferencia) {
        super(transferencia);
        this.transferencia = transferencia;
    }

    public Long getTransferencia() {
        return transferencia;
    }
}
