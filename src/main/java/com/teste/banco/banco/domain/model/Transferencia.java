package com.teste.banco.banco.domain.model;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Transferencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Conta contaOrigem;
    @ManyToOne
    private Conta contaDestino;
    @Enumerated(EnumType.STRING)
    private StatusTransferencia status;
    @Enumerated(EnumType.STRING)
    private TipoTransferencia tipo;
    @CreationTimestamp
    private LocalDateTime dataInicioOperacao;
    @CreationTimestamp
    private LocalDateTime dataConclusaoOperacao;
    private BigDecimal valor;

    public Transferencia() {
    }

    public Transferencia(Conta contaOrigem, Conta contaDestino, BigDecimal valor) {
        this.contaOrigem = contaOrigem;
        this.contaDestino = contaDestino;
        this.valor = valor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Conta getContaOrigem() {
        return contaOrigem;
    }

    public void setContaOrigem(Conta contaOrigem) {
        this.contaOrigem = contaOrigem;
    }

    public Conta getContaDestino() {
        return contaDestino;
    }

    public void setContaDestino(Conta contaDestino) {
        this.contaDestino = contaDestino;
    }

    public StatusTransferencia getStatus() {
        return status;
    }

    public void setStatus(StatusTransferencia status) {
        this.status = status;
    }

    public TipoTransferencia getTipo() {
        return tipo;
    }

    public void setTipo(TipoTransferencia tipo) {
        this.tipo = tipo;
    }

    public LocalDateTime getDataInicioOperacao() {
        return dataInicioOperacao;
    }

    public void setDataInicioOperacao(LocalDateTime dataInicioOperacao) {
        this.dataInicioOperacao = dataInicioOperacao;
    }

    public LocalDateTime getDataConclusaoOperacao() {
        return dataConclusaoOperacao;
    }

    public void setDataConclusaoOperacao(LocalDateTime dataConclusaoOperacao) {
        this.dataConclusaoOperacao = dataConclusaoOperacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    public void definirTipo(){
        if(this.contaOrigem.getCliente().getInstituicao().equals(contaDestino.getCliente().getInstituicao())){
            this.tipo = TipoTransferencia.INTRAINSTITUCIONAL;
            return;
        }
        this.tipo=TipoTransferencia.INTERINSTITUCIONAL;
    }
}
