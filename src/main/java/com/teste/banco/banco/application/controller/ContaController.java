package com.teste.banco.banco.application.controller;

import com.teste.banco.banco.application.dto.ExtratoContaDTOOut;
import com.teste.banco.banco.application.dto.TransferenciaDTOIn;
import com.teste.banco.banco.application.service.ContaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/conta")
public class ContaController {


    private final ContaService contaService;

    public ContaController(ContaService contaService) {
        this.contaService = contaService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BigDecimal> getSaldo(@PathVariable Long id){
        return ResponseEntity.ok().body(contaService.getSaldo(id));
    }

    @GetMapping("/{id}/extrato")
    public ResponseEntity<List<ExtratoContaDTOOut>> getExtrato(@PathVariable Long id){
        return ResponseEntity.ok().body(contaService.getExtrato(id));
    }

    @PostMapping("/transferencia")
    public ResponseEntity<String> realizarTransferencia(@RequestBody TransferenciaDTOIn dto){
        return ResponseEntity.status(HttpStatus.CREATED).body(contaService.realizarTransferencia(dto));
    }


}
