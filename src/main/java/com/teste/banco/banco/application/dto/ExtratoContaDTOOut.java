package com.teste.banco.banco.application.dto;

import com.teste.banco.banco.domain.model.StatusTransferencia;
import com.teste.banco.banco.domain.model.TipoTransferencia;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ExtratoContaDTOOut {

    private Long id;
    private String beneficiario;
    private String pagador;
    private StatusTransferencia status;
    private TipoTransferencia tipo;
    private LocalDateTime dataInicioOperacao;
    private BigDecimal valor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public StatusTransferencia getStatus() {
        return status;
    }

    public void setStatus(StatusTransferencia status) {
        this.status = status;
    }

    public TipoTransferencia getTipo() {
        return tipo;
    }

    public void setTipo(TipoTransferencia tipo) {
        this.tipo = tipo;
    }

    public LocalDateTime getDataInicioOperacao() {
        return dataInicioOperacao;
    }

    public void setDataInicioOperacao(LocalDateTime dataInicioOperacao) {
        this.dataInicioOperacao = dataInicioOperacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getPagador() {
        return pagador;
    }

    public void setPagador(String pagador) {
        this.pagador = pagador;
    }
}
