package com.teste.banco.banco.application.service;

import com.teste.banco.banco.application.dto.ExtratoContaDTOOut;
import com.teste.banco.banco.application.dto.TransferenciaDTOIn;
import com.teste.banco.banco.domain.model.Conta;
import com.teste.banco.banco.domain.model.Transferencia;
import com.teste.banco.banco.domain.model.TransferenciaEvento;
import com.teste.banco.banco.domain.repository.ContaRepository;
import com.teste.banco.banco.domain.repository.TransferenciaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContaService {
    private final ContaRepository contaRepository;
    private final TransferenciaRepository transferenciaRepository;
    private final ModelMapper modelMapper;
    private final ApplicationEventPublisher applicationEventPublisher;

    public ContaService(
            ContaRepository contaRepository,
            TransferenciaRepository transferenciaRepository,
            ModelMapper modelMapper,
            ApplicationEventPublisher applicationEventPublisher) {
        this.contaRepository = contaRepository;
        this.transferenciaRepository = transferenciaRepository;
        this.modelMapper = modelMapper;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public BigDecimal getSaldo(Long id) {
        return getConta(id).getSaldo();
    }

    private Conta getConta(Long id) {
        return contaRepository.findById(id).orElseThrow(()->new RuntimeException("Conta não encontrada"));
    }

    public List<ExtratoContaDTOOut> getExtrato(Long id) {
        return transferenciaRepository.findOfConta(id).stream()
                .map(transferencia -> modelMapper
                        .map(transferencia,ExtratoContaDTOOut.class))
                            .collect(Collectors.toList());
    }

    @Transactional
    public String realizarTransferencia(TransferenciaDTOIn dto) {
        Conta conta = getConta(dto.getConta());
        Transferencia transferencia = new Transferencia(conta,
                contaRepository.findByAgenciaNumeroInstituicao(dto.getAgencia(), dto.getNumero(), dto.getInstituicao())
                        .orElseThrow(() -> new RuntimeException("Conta não encontrada")),
                dto.getValor());
        transferencia = transferenciaRepository.save(transferencia);
        reterValor(conta,dto.getValor());
        applicationEventPublisher.publishEvent(new TransferenciaEvento(transferencia.getId()));
        return "Transferência em analise";
    }

    private void reterValor(Conta conta, BigDecimal valor) {
        conta.subtrairSaldo(valor);
        save(conta);
    }

    public void save(Conta conta) {
        contaRepository.save(conta);
    }
}
