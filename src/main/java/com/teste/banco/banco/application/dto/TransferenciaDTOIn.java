package com.teste.banco.banco.application.dto;

import java.math.BigDecimal;

public class TransferenciaDTOIn {

    private Long conta;
    private String agencia;
    private String numero;
    private Long instituicao;
    private BigDecimal valor;

    public Long getConta() {
        return conta;
    }

    public void setConta(Long conta) {
        this.conta = conta;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Long instituicao) {
        this.instituicao = instituicao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
