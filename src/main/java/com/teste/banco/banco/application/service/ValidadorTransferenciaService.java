package com.teste.banco.banco.application.service;

import com.teste.banco.banco.domain.model.Conta;
import com.teste.banco.banco.domain.model.StatusTransferencia;
import com.teste.banco.banco.domain.model.Transferencia;
import com.teste.banco.banco.domain.model.TransferenciaEvento;
import com.teste.banco.banco.domain.repository.TransferenciaRepository;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ValidadorTransferenciaService {

    private final TransferenciaRepository transferenciaRepository;
    private final ContaService contaService;

    public ValidadorTransferenciaService(TransferenciaRepository transferenciaRepository, ContaService contaService) {
        this.transferenciaRepository = transferenciaRepository;
        this.contaService = contaService;
    }

    @Async
    @EventListener
    public void validarTransferencia(TransferenciaEvento evento){
        Transferencia transferencia = transferenciaRepository.findById(evento.getTransferencia()).orElseThrow(() -> new RuntimeException("Transferencia não encontrada"));
        validarLimites(transferencia);
        aplicarTaxas(transferencia);
        finalizarTransferencia(transferencia);
    }

    private void validarLimites(Transferencia transferencia) {
        if(Objects.isNull(transferencia.getTipo().getValorMaximo())) return;
        if(transferencia.getValor().compareTo(transferencia.getTipo().getValorMaximo())<0){

            reverterTranferencia(transferencia,StatusTransferencia.FALHA_POR_EXCEDER_LIMITE, "O limite para transferencias desse tipo é de "+transferencia.getTipo().getValorMaximo());
            throw new RuntimeException();
        }
    }

    private void reverterTranferencia(Transferencia transferencia, StatusTransferencia status, String message) {
        transferencia.setStatus(status);
        transferenciaRepository.save(transferencia);
        Conta contaOrigem = transferencia.getContaOrigem();
        contaOrigem.adicionarSaldo(transferencia.getValor());
        contaService.save(contaOrigem);
        throw new RuntimeException(message);
    }

    private void aplicarTaxas(Transferencia transferencia) {
        Conta contaOrigem = transferencia.getContaOrigem();
        try{
            contaOrigem.subtrairSaldo(transferencia.getTipo().getTaxa());
        }catch (RuntimeException e){
            reverterTranferencia(transferencia,StatusTransferencia.FALHA_POR_FALTA_DE_SALDO,"Saldo insuficiente para aplicação das taxas");
        }
        contaService.save(contaOrigem);
    }


    private void finalizarTransferencia(Transferencia transferencia) {
        transferencia.setStatus(StatusTransferencia.REALIZADA);
        Conta contaDestino = transferencia.getContaDestino();
        contaDestino.adicionarSaldo(transferencia.getValor());
        contaService.save(contaDestino);
        transferenciaRepository.save(transferencia);
    }
}
